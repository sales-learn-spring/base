package com.backend.projeto.config;

import com.backend.projeto.entity.User;
import com.backend.projeto.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DataInitializr implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        List<User> users = this.userRepository.findAll();

        if (users.isEmpty()) {
            createUsers("Felipe", "felipe@gmail.com", "102030");
            createUsers("Felipe Ferreira", "felipeferreira@gmail.com", "102030");
            createUsers("Felipe Sales", "felipesales@gmail.com", "102030");
        }
    }

    private void createUsers(String nome, String email, String password ) {

        User user = new User(nome, email, password);
        userRepository.save(user);
    }
}
