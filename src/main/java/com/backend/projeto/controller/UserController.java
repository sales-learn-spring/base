package com.backend.projeto.controller;

import com.backend.projeto.entity.User;
import com.backend.projeto.repository.UserRepository;
import com.backend.projeto.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping
    public List<User> list(@RequestParam(value = "page", defaultValue = "0") int page, @RequestParam(value = "size", defaultValue = "1") int size) {

        return this.userService.findAllPageble(page, size).toList();
    }

    @PostMapping
    public User create(@RequestBody User user) {
        return this.userService.create(user);
    }

    @PutMapping
    public User edit(@RequestBody User user) {
        return this.userService.update(user);
    }

    @DeleteMapping(path = {"/{id}"})
    public void delete(@PathVariable long id) {
        this.userService.delete(id);
    }

    @GetMapping(path = {"/{id}"})
    public User findById(@PathVariable long id) {
        return this.userService.findById(id).get();
    }
}
